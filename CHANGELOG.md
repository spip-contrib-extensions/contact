# Changelog du plugin Contact Avancé

## 2.0.0 - 2025-02-27

### Changed

- Compatible SPIP 4.2 minumum
- Chaînes de langue au format SPIP 4.1+