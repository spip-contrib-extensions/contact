<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-contact?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// C
	'contact_description' => 'Este plugin permite compor livremente um formulário de contato (nome, sobrenome, endereço, telefone...) e escolher a lista de destinatários que receberão as informações coletadas.',
	'contact_nom' => 'Formulário de contato avançado',
	'contact_slogan' => 'Esta extenção oferece um formulário de contato com múltiplas opções',
];
