<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-contact?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'contact_description' => 'Dieses Plugin ermöglicht  die Einrichtung eines Kontaktformulars (Name, Vorńame, Adresse, Telefon,...) und die Auswahl von Empfängern, welche die erfassten Daten per Mail erhalten.',
	'contact_nom' => 'Erweitertes Kontaktformular',
	'contact_slogan' => 'Dieses Plugin bietet ein Kontaktformular mit mehreren Optionen.',
];
