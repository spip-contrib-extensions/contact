<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-contact?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// C
	'contact_description' => 'هذا التمديد يوفر شكل من أشكال الاتصال مع خيارات متعددة',
	'contact_nom' => 'إستمارة الاتصال المتقدمة',
	'contact_slogan' => 'هذا التمديد يوفر شكل من أشكال الاتصال مع خيارات متعددة',
];
