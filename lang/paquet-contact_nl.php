<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-contact?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// C
	'contact_description' => 'Maak met deze plugin een eigen contactformulier (naam, voornaam, adres, telefoon, ...) en biedt een keuzelijst van geadresseerden die de verzamelde informatie per mail ontvangen.',
	'contact_nom' => 'Geavanceerd contactformulier',
	'contact_slogan' => 'Deze uitbreiding biedt een contactformulier met diverse opties',
];
