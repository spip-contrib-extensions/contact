<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-contact?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// C
	'contact_description' => 'Tento zásuvný modul vám umožňuje vytvoriť vlastný kontaktný formulár (meno, priezvisko, adresa, telefón, atď) a vybrať príjemcov, ktorí dostanú zistené údaje e-mailom.',
	'contact_nom' => 'Rozšírený kontaktný formulár',
	'contact_slogan' => 'Toto rozšírenie ponúka kontaktný formulár s viacerými možnosťami',
];
